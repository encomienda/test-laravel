<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('user_id'); // this is the column
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade'); // this one will make the user_id column as foreign key

            $table->unsignedInteger('post_id'); // this is the column
            $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade'); // this one will make the post_id column as foreign key

            $table->text('text');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
